[**Distributor Sarung Tangan Sepeda**](https://avelio.com/distributor-sarung-tangan-sepeda/)

Ini adalah API untuk reseller [sarung tangan sepeda](https://avelio.com/) Avelio.

Data yang dapat diambil:
1. SKU Produk
2. Nama Produk
3. Deskripsi
4. Harga
5. Varian
6. Stok
7. Info promo
